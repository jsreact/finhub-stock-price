# NASDAQ Live Stock Price

## Description
This project is a full stack application. The main idea of the website is to provided the user with the real time stock price according to the stock ticker the user wants in NASDAQ exchange. The real time data will be provided by an API https://finnhub.io/ to the server which will in turn pass the information to the user.

## Authors
1. Davit Voskerchyan - myself
2. Mr. Yildirim
