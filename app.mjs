/**
 * This file is the entry point of the application.
 * It acts as the server and handles all the requests.
 * @date 2022/10/31
 */
import express from "express";
import FinhubApiController from "./controller/finhubAPI.controller.mjs"
import Nasdaq from "./fileio/Nasdaq.mjs"

let nasdaq = new Nasdaq();
const nasdaqData = await nasdaq.getStockName();

const app = express();
const PORT = 3000;

// Define public folder and accept json data
app.use(express.static('public'))
app.use(express.json())



// This path returns all nasdaqData gotten from files\basicNasdaq.json 
app.get("/nasdaq/tickers",(req,res,next)=>{
  if(nasdaqData){
    res.status(200).json(nasdaqData)
  }else{
    // Server error sicne nasdaqData is null
    res.status(500).json({message:"No data"})
  }
})

// Route to FinHub router file
app.get("/api/ticker/", FinhubApiController.apiGetTickerRealTimePrice)
app.get("/api/ticker/price", FinhubApiController.apiGetTickerRealTimePrice)

// Default to 404
app.use((req,res,next) =>{
  res.sendStatus(404)
})

/**
 * @param {string} PORT Number of the server listening
 */
app.listen(PORT, () => {
  console.log(`${PORT} is start to listening`);
});
