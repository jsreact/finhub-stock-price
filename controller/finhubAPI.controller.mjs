/**
 * This file controlls the business Logic of the FinHub.io API.
 * @date 2022/28/10
 * @author Davit Voskerchyan
 */

import { key } from "./keys.mjs";
import fetch from "node-fetch";
/**
 * Class Of Controller
 */
export default class FinhubApiController {
    /**
     * This method handles an HTTP GET request to get real time stock price 
     * from FinHub API by using a query. (i.e ?ticker=[stock_symbol])
     * 
     * The query is passed as a parameter in the URL called stock.
     * @param {*} req : HTTP Request from user
     * @param {*} res : HTTP Response to be created
     * @returns An HTTP Response with current symbol price
     */
    static async apiGetTickerRealTimePrice(req, res) {
        try {
            // Get ticker and create URL
            let stock;
            if (req.query.stock) stock = req.query.stock
            else throw new Error("Ticker is undefined in query - GET")

            const url = `https://finnhub.io/api/v1/quote?symbol=${stock.toUpperCase()}&exchange=US&token=${key.key}`

            // Get Price from FinHub
            const finhub_resp = await fetch(url)

            // Will sometimes return 200 with no data
            if (!finhub_resp.ok) { throw new Error("An error occurred while fetching the stock price") }

            const price = await finhub_resp.json()            
            // If no price is returned, than ticker wasn't found
            if (price.c == 0) { throw new Error("An error occurred while fetching the stock price") }
            
    
            // Build response
            const response = {
                ticker: stock,
                price: price.c || -1
            }

            // Send response as json
            res.status(200).json(response)
        } catch (e) {
            res.status(404).send(e.message)
            console.error(e.message)
        }
    }

    /**
      * This method handles an HTTP POST request to get real time stock price 
      * from FinHub API by using a query. (i.e ?ticker=[stock_symbol])
      * 
      * @param {*} req : HTTP Request from user
      * @param {*} res : HTTP Response to be created
      * @returns An HTTP Response with current symbol price
      */
    static async apiPostTickerRealTimePrice(req, res, next) {
        try {
            // Get ticker and create URL
            let stock;
            if (req.body.stock) stock = req.body.stock
            else throw new Error("Ticker in body isn't defined - POST")

            const url = `https://finnhub.io/api/v1/quote?symbol=${stock.toUpperCase()}&exchange=US&token=${key.key}`
            // Get Price from FinHub
            const finhub_resp = await fetch(url)
            if (!finhub_resp.ok) { throw new Error("An error occurred while fetching the stock price") }
            const price = await finhub_resp.json()

            // Build response
            const response = {
                ticker: stock,
                price: price.c || -1
            }
            // Send response as json
            res.status(200).json(response)
        } catch (e) {
            res.status(404).send(e.message)
            console.error(e.message)
        }
    }
}