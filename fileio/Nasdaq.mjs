/**
 * This file implements the Singleton Nasdaq class.
 * @date 2022/10/24
 */
import {readJson } from '../fileio/fileReader.mjs'

let singleton = null;
const PATH = './files/basicNasdaq.json'
class Nasdaq{
    constructor(){
        if(singleton == null){
            singleton = this;
        }
        return singleton
    }
    async getStockName(){
        if(this.nasdaq){
            return this.nasdaq;
        }
        this.nasdaq = await readJson(PATH)
        return this.nasdaq
    }
}

export default Nasdaq;