/**
 * @description This file functionality basiccally read a file and returns wiht a json object. Before reading checks the file and path exists then reads it.
 * @param {author} Cuneyt YILDIRIM
 * @date 2022-10-19
 */


import fs from "fs/promises";

/**
 * Checks the current file's location
 * @param {location of the json file} path
 */
async function isPath(path) {
  try {
    await fs.access(path);
    return true;
  } catch (error) {
    console.error("The Path does not correct " + error);
    return false;
  }
}

/**
 * Checks the current file is exist
 * @param {location of the json file} path
 */
async function verFile(path) {
  try {
    let status = await fs.stat(path);
    return status.isFile();
  } catch (error) {
    console.error("The File does not correct " + error);
    return false;
  }
}

/**
 * @description {Reads the file and convert to utf-8 format}
 * @param {location of the json file} path
 * @return {data that read from provided path}
 * 
 */
async function readFile(path) {
  if ((await verFile(path)) && (await isPath(path))) {
    try {
      let data = await fs.readFile(path,'utf-8');
      return data;
    } catch (error) {
      console.error(error);
      throw new Error("A problem has occurred while file reading");
    }
  }
}

/**
 * @description {After reading the file convert to JSON file.} 
 * @param {location of the json file} path
 * @return {Returns with JSON object}
 */
async function readJson(path) {
  try {
    let data = await readFile(path);
    return JSON.parse(data);

  } catch (error) {
    console.error("Error Occured while file converting to JSON object");
    process.exit(9)
  }
}


export { readJson, readFile,verFile,isPath };
