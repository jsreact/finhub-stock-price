/**
 * The function fetches all nasdaq tickers than creates options for datalist.
 * The variable is a promise to be able to export it.
 */
const nasdaq_options_fragement = (async () => {
    let fragement = new DocumentFragment()
    let nasdaq_fetch = await fetch("/nasdaq/tickers")
    let nasdaq_json = await nasdaq_fetch.json()

    // Create datalist options
    nasdaq_json.forEach(stock => {
        const option = document.createElement("option")
        option.value = stock.symbol
        fragement.appendChild(option)
    })
    return [fragement, nasdaq_json]
})()


/**
 * This listener waits for DOMContentLoaded to append all Nasdaq options.
 */
document.addEventListener("DOMContentLoaded", async () => {
    let [fragement, json]= await nasdaq_options_fragement
    document.querySelector("#allTickers").appendChild(fragement)
})

/**
 * This function returns all the nasdaq data from nasdaq.json.
 * @returns nasdaq name list 
 */
export const nasdaq_name = (async () => {
    let [fragment, json] = await nasdaq_options_fragement
    return json;
})