// Server entry point
import { nasdaq_name } from "./asyncNasdaqTickers.mjs";

const SERVER_URL = "http://localhost:3000"

document.addEventListener("DOMContentLoaded", init);

function init() {
    document.querySelector("#submitBtn").addEventListener("click", searchTicker)
    // Prevent page reload on form submission
    document.querySelector("#form").addEventListener("submit", (e) => {
         e.preventDefault()
         searchTicker() 
    })
}


/**
 *This function get the companyName from nasdaq.json file and displays the result of the company name along with the stock's price
 * @param {*} stockSymbol  
 */
 async function companyName(stockSymbol){
    let json = await nasdaq_name()
    return json.find((stockInfo) =>{
        return stockInfo.symbol === stockSymbol.toUpperCase()
    })
}
/**
 * This function will fetch the information about the ticker requested
 */
async function searchTicker() {
    const stock = document.querySelector("#stock").value
    const resultText = document.querySelector("#display_result")
    resultText.style.visibility = 'visible'
    resultText.textContent = "Loading..."

    try {
        let stock_info = await fetch(`${SERVER_URL}/api/ticker/price/?stock=${stock}`)
        if (!stock_info.ok) {
            throw new Error("The error occurred while fetching the stock price")
        }
        let stock_result = await stock_info.json();
        let company  = await companyName(stock)
        placeData(stock_result, stock, null, resultText,company.description )

    } catch (e) {
        let error = "Not Found"
        placeData(null, stock, error, resultText,null)
    }
}


/**
 * This function placed the stock price data into the html element
 * @param {*} jsonData json data based on the stock price
 * @param {*} stock ticker name 
 * @param {*} error error message in case the fetch failed
 */
function placeData(jsonData, stock, error, resultText,companyName) {
    let stock_price = document.querySelector("#stock_price")
    let price_text = document.querySelector(".price-text")

    if (jsonData == null) {
        resultText.textContent = `THE CURRENT PRICE OF ${stock} is: ${error}`
        price_text.classList.add("error")
        return
    }

    // stock_name = stock
    stock_price = jsonData.price
    price_text.classList.remove("error")
    resultText.textContent = `THE CURRENT PRICE OF ${companyName} is: $ ${stock_price}`

}
