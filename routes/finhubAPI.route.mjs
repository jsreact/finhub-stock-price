/**
 * This file specifies the specific routes and its corressponding callbacks for
 * Finhub API.
 * @date 2022/28/10
 * @author Davit Voskerchyan
 */
import express from "express"
import FinhubApiController from "../controller/finhubAPI.controller.mjs"

const router = express.Router()


router.route("/").get(FinhubApiController.apiGetTickerRealTimePrice)

router.route("/price")
                    .post(FinhubApiController.apiPostTickerRealTimePrice)
                    .get(FinhubApiController.apiGetTickerRealTimePrice)

// Exports
export default router;
